/**
 * Key/value pair.
 */
#ifndef __kvpair_h__
#define __kvpair_h__

#define TYPE_STRING         1
#define TYPE_INT            2
#define TYPE_DOUBLE         3
#define TYPE_BOOLEAN        4

/**
 * Key/value pair structure.
 */
typedef struct {
    int idx_key;            /* Indexed key after adding to dictionary */
    char *key;              /* Key string */
    char type;              /* A type - one of TYPE_* definitions */

    /* Value union */
    union {
        char *v_string;     /* String value */
        long v_int;         /* Integer long-sized value */
        double v_double;    /* Double value */
        char v_boolean;     /* Boolean value */
    } value;
} kvpair_t;

/* Write JSONs separator to a file */
int kv_write_separator(int fd);

/* Write K/V pair to a file */
int kv_write(int fd, kvpair_t *kv);

/* Read K/V pair from a file */
int kv_read(int fd, kvpair_t *kv);

#endif /* kvpair.h */
