
BIN=codec
OBJECTS=codec.o json_reader.o keydict.o kvpair.o
HEADERS=json_reader.h keydict.h kvpair.h
CC=gcc
CFLAGS=-Wall -O2

.PHONY: all clean

all: $(BIN)

$(BIN): %: %.o $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $@

%.o: %.c $(HEADERS)
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f $(OBJECTS) $(BIN)
