/**
 * Simplified JSON parser header file.
 */
#ifndef __json_reader_h__
#define __json_reader_h__

#define JSON_OK             0
#define JSON_MEMORY_ERR     1
#define JSON_CANT_READ      2
#define JSON_SYNTAX_ERR     3

#include <sys/types.h>
#include <stdio.h>

#include "kvpair.h"

/* JSON file descriptor */
typedef struct {
    int fd;             /* File descriptor */
    int open_bracket;   /* Is opening bracket parsed */
    int end_of_json;    /* Is end of JSON reached */
    int end_of_file;    /* Is end of file reached */
    off_t state;        /* Parsing helper variable - saved file offset */
} json_t;

/* JSON parsing last error */
extern int json_error;

/* JSON parsing error messages */
extern const char * json_str_error[];

/* JSON error code to error message */
const char *json_strerr(int errno);

/* Prints out a JSON-formatted K/V pair to a file */
void json_print_kv(FILE *output, kvpair_t *kv);

/* Open a JSON file */
json_t *json_open(const char *filename);

/* Reset end_of_json flag for next JSON */
void json_reset(json_t *json);

/* Read from a JSON file */
kvpair_t *json_read(json_t *json);

/* Close a JSON file */
void json_close(json_t *json);

#endif /* json_reader.h */
