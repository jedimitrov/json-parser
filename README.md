
# Build

Type ```make```

# Usage

#### JSON Encoding
```./codec -e <json_input_file> <binary_file> <dict_file>```

#### JSON Decoding
```./codec -e <json_output_file> <binary_file> <dict_file>```

# Test

#### Encode JSON file to a binary/dictionary files
```./codec -e test/test.json test/test.bin test/test.dict```

#### Decode binary/dictionary files to a JSON file
```./codec -d test/output.json test/test.bin test/test.dict```

# Notes

During development I tried to stick as close as possible to the requirements and for that reason I didn't code some of the optimizations that could have been done. Many things can be further optimized in the code, such as the memory allocation mechanism when reading strings and the actual reading and parsing of the JSON file. I am ready to comment on any aspect of development and optimization.

I chose to write my own simplified JSON parser to meet the requirement that records be read one at a time and save memory.

The test JSON file is intentionally badly formatted to demonstrate the capabilities of the JSON parser.

I hope I have solved the task in a satisfactory way. I remain open to a further interview.
