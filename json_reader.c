/**
 * Simplified JSON parser.
 */
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>

#include "json_reader.h"

#define  END_OF_FILE                (1 << 8)    /* A value out of char range */

#define  json_set_err(errno)        { json_error = errno; goto err_finalize; }
#define  json_ret_err(errno, res)   { json_error = errno; return res; }

#define  is_digit(c)                ((c) >= '0' && (c) <= '9')
#define  is_alpha(c)                (((c) >= 'a' && (c) <= 'z') || ((c) >= 'A' && (c) <= 'Z'))

const char *json_unknown_error = "JSON unknown error";
const char *json_str_error[] = {
    "JSON is OK",
    "JSON memory allocation error",
    "Unable to read from JSON file",
    "JSON syntax error"
};

int json_error = JSON_OK;


const char *json_strerr(int errno) {
    if (errno >= JSON_OK && errno <= JSON_SYNTAX_ERR)
        return json_str_error[errno];
    return json_unknown_error;
}


/*
 * Static function.
 * Read next character, skip whitespaces.
 */
static inline int json_readc(json_t *json) {
    unsigned char c;
    int count;
    while ((count = read(json->fd, &c, 1)) > 0) {
        if (c != ' ' && c != '\r' && c != '\n' && c != '\t')
            return c;
        }
    if (count == 0)
		return END_OF_FILE;
    json_ret_err(JSON_CANT_READ, -1);
}


/*
 * Static function.
 * Save current file offset.
 */
static inline int json_save_state(json_t *json) {
    json->state = lseek(json->fd, 0, SEEK_CUR);
    return (json->state == (off_t) - 1 ? -1 : 0);
}


/*
 * Static function.
 * Restore previously saved file offset.
 */
static inline int json_restore_state(json_t *json) {
    return (lseek(json->fd, json->state, SEEK_SET) == (off_t)-1 ? -1 : 0);
}


/*
 * Static function.
 * Prints out escaped JSON string to a file.
 */
static void json_print_string(FILE *output, const char *s)
{
    int i;

    for (i=0; s[i]; i++) {
        switch (s[i]) {
            case '\n':  fputs("\\n", output); break;
            case '\r':  fputs("\\r", output); break;
            case '\\':  fputs("\\\\", output); break;
            case '"':   fputs("\\\"", output); break;
            default:    fputc(s[i], output); break;
        }
    }
}


void json_print_kv(FILE *output, kvpair_t *kv)
{
    /* Prints out a key first */
    fputc('"', output);
    json_print_string(output, kv->key);
    fputs("\": ", output);

    /* Prints out a value according to its type */
    switch (kv->type) {
        case TYPE_STRING:
            fputc('"', output);
            json_print_string(output, kv->value.v_string);
            fputc('"', output);
            break;

        case TYPE_INT:
            fprintf(output, "%ld", kv->value.v_int);
            break;

        case TYPE_DOUBLE:
            fprintf(output, "%lf", kv->value.v_double);
            break;

        case TYPE_BOOLEAN:
            fprintf(output, "%s", (kv->value.v_boolean ? "TRUE" : "FALSE"));
            break;

        default:
            fputs("NULL", output);
    }

}


json_t *json_open(const char *filename)
{
    /* Create and initialize JSON structure */
    json_t *json = (json_t*)malloc(sizeof(json_t));
    if (json == NULL)
        json_set_err(JSON_MEMORY_ERR);
    json->open_bracket = 0;
    json->end_of_json = 0;
	json->end_of_file = 0;

    /* Open file for reading */
    json->fd = open(filename, O_RDONLY);
    if (json->fd == -1)
        json_set_err(JSON_CANT_READ);

    return json;

err_finalize:
    if (json != NULL)
        free(json);
    return NULL;
}


/**
 * Reset end_of_json flag for next JSON.
 */
inline void json_reset(json_t *json)
{
	json->end_of_json = 0;
	json->open_bracket = 0;
}


/*
 * Static function.
 * Read string from JSON file.
 */
#define STRING_CHUNK   64
static char *json_read_string(json_t *json)
{
    int c = 0, ptr = 0, size = 0;
    char *string = NULL, *new_string;

    /* Read first quote */
    c = json_readc(json);
    if (c != '"')
        return NULL;

    /* Read all characters in the string, one by one */
    for(;;) {
        if (read(json->fd, &c, 1) <= 0)
            json_set_err(JSON_CANT_READ);

        /* Return string if a closing quote is reached */
        if (c == '"') {
            new_string = (char*)realloc(string, ptr + 1);
            if (new_string == NULL)
                json_set_err(JSON_MEMORY_ERR);
            new_string[ptr] = '\0';
            return new_string;
        }

        /* Parse (some of the) escape sequences */
        if (c == '\\') {
            if (read(json->fd, &c, 1) <= 0)
                json_set_err(JSON_CANT_READ);
            switch (c) {
                case 'n' : c = '\n'; break;
                case 'r' : c = '\r'; break;
                case '\\': c = '\\'; break;
                case '"' : c = '"' ; break;
                default:
                    json_set_err(JSON_SYNTAX_ERR);
            }
        }

        /* Extend the array for the next character */
        if (ptr >= size) {
            new_string = (char*)realloc(string, size + STRING_CHUNK);
            if (new_string == NULL)
                json_set_err(JSON_MEMORY_ERR);
            string = new_string;
            size += STRING_CHUNK;
        }

        string[ptr++] = c;
    }

err_finalize:
    if (string != NULL)
        free(string);
    return NULL;
}

/*
 * Static function.
 * Read double value from JSON file.
 */
#define DOUBLE_BUF  21
static double json_read_double(json_t *json, int *err)
{
    int c, pos = 0, point = 0;
    char buf[DOUBLE_BUF];

    *err = 1;

    /* Read first character of the value, should be either a digit, or a '-' sign */
    c = json_readc(json);
    if (!is_digit(c) && c != '-')
        return 0;

    do {
        /* Save character */
        buf[pos++] = c;

        /* Buffer overflow check */
        if (pos >= DOUBLE_BUF)
            return 0;
        buf[pos] = '\0';

        /* Read next character */
        if (read(json->fd, &c, 1) < 1)
            return 0;

        /* Point character is allowed only once */
        if (c == '.') {
            if (point)
                return 0;
            point = 1;
        }
    } while(is_digit(c) || c == '.');

    /* Not a double */
    if (!point)
        return 0;

    /* Push back last character */
    if (lseek(json->fd, -1, SEEK_CUR) == (off_t)-1)
        return 0;

    *err = 0;
    return atof(buf);
}

#define INT_BUF     20
static long json_read_int(json_t *json, int *err)
{
    int c, pos = 0;
    char buf[INT_BUF];

    *err = 1;

    /* Read first character of the value, should be either a digit, or a '-' sign */
    c = json_readc(json);
    if (!is_digit(c) && c != '-')
        return 0;

    do {
        /* Save character */
        buf[pos++] = c;

        /* Buffer overflow check */
        if (pos >= INT_BUF)
            return 0;
        buf[pos] = '\0';

        /* Read next character */
        if (read(json->fd, &c, 1) < 1)
            return 0;
    } while(is_digit(c));

    /* Push back last character */
    if (lseek(json->fd, -1, SEEK_CUR) == (off_t)-1)
        return 0;

    *err = 0;
    return atol(buf);
}


static char json_read_boolean(json_t *json)
{
    char buf[5];

    /* Skip whitespaces, read first character */
    buf[0] = json_readc(json);
    /* Return if not a letter */
    if (!is_alpha(buf[0]))
        return -1;

    /* Read 3 more characters (4 in total) */
    if (read(json->fd, &buf[1], 3) < 3)
        return -1;
    /* Check for TRUE value */
    if (strncasecmp(buf, "true", 4) == 0)
        return 1;

    /* Read 1 more character (5 in iotal) */
    if (read(json->fd, &buf[4], 1) < 1)
        return -1;
    /* Check for FALSE value */
    if (strncasecmp(buf, "false", 5) == 0)
        return 0;

    /* Not a boolean */
    return -1;
}


kvpair_t *json_read(json_t *json)
{
    kvpair_t *kv = NULL;
    int c, err;

    /* Expect an open bracket in the beginning */
    if (!json->open_bracket) {
        c = json_readc(json);
		if (c == END_OF_FILE) {
			json->end_of_file = 1;
			return NULL;
		}

        if (c != '{')
            json_set_err(JSON_SYNTAX_ERR);
        json->open_bracket = 1;

        /* Check for empty JSON */
        json_save_state(json);
        c = json_readc(json);
        if (c == '}') {
            json->end_of_json = 1;
            return NULL;
        }
        json_restore_state(json);
    }

    /* Allocate and initialize a new K/V pair */
    kv = (kvpair_t*)malloc(sizeof(kvpair_t));
    if (!kv)
        json_set_err(JSON_MEMORY_ERR);
    memset(kv, 0, sizeof(*kv));
    kv->idx_key = -1;

    /* Read key */
    kv->key = json_read_string(json);
    if (kv->key == NULL)
        json_set_err(JSON_SYNTAX_ERR);

    /* Read a colon K/V separator */
    c = json_readc(json);
    if (c != ':')
        json_set_err(JSON_SYNTAX_ERR);

    json_save_state(json);

    /* Try to read string */
    kv->value.v_string = json_read_string(json);
    if (kv->value.v_string != NULL) {
        kv->type = TYPE_STRING;
        goto value_ok;
    }

    json_restore_state(json);
    /* Try to read double */
    err = 0;
    kv->value.v_double = json_read_double(json, &err);
    if (!err) {
        kv->type = TYPE_DOUBLE;
        goto value_ok;
    }

    json_restore_state(json);
    /* Try to read integer */
    err = 0;
    kv->value.v_int = json_read_int(json, &err);
    if (!err) {
        kv->type = TYPE_INT;
        goto value_ok;
    }

    json_restore_state(json);
    /* Try to read boolean */
    kv->value.v_boolean = json_read_boolean(json);
    if (kv->value.v_boolean != -1) {
        kv->type = TYPE_BOOLEAN;
        goto value_ok;
    }

    json_set_err(JSON_SYNTAX_ERR);

value_ok:
    /* Check for end of JSON or next K/V pair */
    c = json_readc(json);
	if (c == END_OF_FILE) {
		json->end_of_file = 1;
		return kv;
	}
    if (c == '}')
        json->end_of_json = 1;
    else if (c != ',')
        json_set_err(JSON_SYNTAX_ERR);

    return kv;

err_finalize:
    if (kv) {
        if (kv->key)
            free(kv->key);
        if (kv->type == TYPE_STRING && kv->value.v_string != NULL)
            free(kv->value.v_string);
        free(kv);
    }
    return NULL;
}


void json_close(json_t *json)
{
    close(json->fd);
}
