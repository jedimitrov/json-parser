#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "kvpair.h"

#define  e_write(fd, buf, count)    { if (write(fd, buf, (count)) < (count)) return -1; }
#define  e_read(fd, buf, count)     { if (read(fd, buf, (count)) < (count)) return -1; }

int kv_write_separator(int fd)
{
    int separator = -1;
    e_write(fd, &separator, sizeof(separator));
    return 0;
}

int kv_write(int fd, kvpair_t *kv)
{
    unsigned char type;
    long lv;                        // Long value
    int len;                        // Integer length in bytes
    int i;

    /* Write key index */
    e_write(fd, &kv->idx_key, sizeof(kv->idx_key));

    switch (kv->type) {
        case TYPE_STRING:
            /* Write type */
            type = TYPE_STRING;
            e_write(fd, &type, 1);

            /* Write a null-terminated string */
            e_write(fd, kv->value.v_string, strlen(kv->value.v_string) + 1);
            break;

        case TYPE_INT:
            /* Find the minimum number of bytes needed to store this integer */
            lv = kv->value.v_int;
            len = sizeof(lv);
            for (i=len; i>0; i--)
                if ((lv >> 56) == 0) {
                    len--;
                    lv <<= 8;
                } else
                    break;

            /* Encode type and number of bytes (length) for this integer */
            type = TYPE_INT | (len << 4);
            e_write(fd, &type, 1);

            /* Write value to a file */
            if (len > 0)
                e_write(fd, &kv->value.v_int, len);

            break;

        case TYPE_DOUBLE:
            /* Write type and value */
            type = TYPE_DOUBLE;
            e_write(fd, &type, 1);
            e_write(fd, &kv->value.v_double, sizeof(kv->value.v_double));

            break;

        case TYPE_BOOLEAN:
            /* Write type and value encoded in one byte */
            type = TYPE_BOOLEAN | (kv->value.v_boolean << 4);
            e_write(fd, &type, 1);

            break;
    }

    return 0;
}


int kv_read(int fd, kvpair_t *kv)
{
    unsigned char type;
    char c;
    int len;

    /* Read key index from a file */
    if (read(fd, &kv->idx_key, sizeof(kv->idx_key)) < sizeof(kv->idx_key))
        return -1;
    if (kv->idx_key == -1)
        return 0;

    /* Read value type, and possibly additional information for integer/boolean */
    e_read(fd, &type, 1);

    /* Extract type only */
    kv->type = type & 0x0f;

    switch(kv->type) {

        /* Decode a string */
        case TYPE_STRING:
            kv->value.v_string = NULL;
            len = 0;
            do {
                e_read(fd, &c, 1);
                kv->value.v_string = (char*)realloc(kv->value.v_string, len+1);
                if (kv->value.v_string == NULL)
                    return -1;
                kv->value.v_string[len++] = c;
            } while(c);
            break;

        /* Decode an integer */
        case TYPE_INT:
            len = type >> 4;
            kv->value.v_int = 0;
            if (len > 0)
                e_read(fd, &kv->value.v_int, len);
            break;

        /* Decode a double */
        case TYPE_DOUBLE:
            e_read(fd, &kv->value.v_double, sizeof(kv->value.v_double));
            break;

        /* Decode a boolean */
        case TYPE_BOOLEAN:
            kv->value.v_boolean = type >> 4;
            break;

        default:
            return -1;
    }

    return 1;
}
