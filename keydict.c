#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "keydict.h"

#define  e_write(fd, buf, count)    { if (write(fd, buf, (count)) < (count)) return -1; }
#define  e_read(fd, buf, count)     { if (read(fd, buf, (count)) < (count)) return -1; }

void dict_init(keydict_t *dict)
{
    dict->keys = NULL;
    dict->size = 0;
    dict->last_index = 1;
}


void dict_destroy(keydict_t *dict)
{
    int i;

    for (i=0; i<dict->size; i++)
        free(dict->keys[i].key);
    free(dict->keys);
}


int dict_add(keydict_t *dict, char *key, int index)
{
    keyitem_t *items;

    /* Create room for the new K/V pair */
    items = (keyitem_t*)realloc(dict->keys, sizeof(keyitem_t) * (dict->size + 1));
    if (items == NULL)
        return -1;

    /* Initialize key */
    items[dict->size].key = key;

    /* Initialize index by either a parameter passed, or automaticaly */
    items[dict->size].index = (index > -1 ? index : dict->last_index);

    /* Save extended array and adjust size */
    dict->keys = items;
    dict->size++;

    /* Return index */
    if (index > -1)
        return index;
    return dict->last_index++;
}


int dict_write(int fd, keydict_t *dict)
{
    int i;

    /* Write all K/V pairs to a file - index as integer, key as null-terminated string */
    for (i=0; i<dict->size; i++) {
        e_write(fd, &dict->keys[i].index, sizeof(dict->keys[i].index));
        e_write(fd, dict->keys[i].key, strlen(dict->keys[i].key) + 1);
    }

    return 0;
}


int dict_read(int fd, keydict_t *dict)
{
    int index, len;
    char *key = NULL, c;

    /* Initialize dictionary */
    dict_init(dict);

    /* Read index first */
    while(read(fd, &index, sizeof(index)) == sizeof(index)) {
        len = 0;
        key = NULL;

        /* Read string */
        do {
            e_read(fd, &c, 1);
            key = (char*)realloc(key, len + 1);
            if (key == NULL)
                return -1;
            key[len++] = c;
        } while (c);

        /* Add to dictionary */
        dict_add(dict, key, index);
    }

    return 0;
}


char *dict_get(keydict_t *dict, int index)
{
    int i;

    /* Traverse dictionary, look for index */
    for (i=0; i<dict->size; i++) {
        if (dict->keys[i].index == index)
            return dict->keys[i].key;
    }

    return NULL;
}
