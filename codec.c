#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include "json_reader.h"
#include "keydict.h"

#define  USAGE      "Usage: ./codec <-e | -d> <json_file> <binary_file> <dict_file>"

void err_sys(const char *msg);

void json_encode(const char *json_file, const char *bin_file, const char *dict_file);
void json_decode(const char *json_file, const char *bin_file, const char *dict_file);


int main(int argc, char **argv)
{
    if (argc != 5)
        err_sys(USAGE);

    /* Encode option */
    if (strcmp(argv[1], "-e") == 0)
        json_encode(argv[2], argv[3], argv[4]);

    /* Decode option */
    else if(strcmp(argv[1], "-d") == 0)
        json_decode(argv[2], argv[3], argv[4]);

    else
        err_sys(USAGE);

    return 0;
}

void json_encode(const char *json_file, const char *bin_file, const char *dict_file)
{
    json_t *json;
    kvpair_t *kv = NULL;
    keydict_t dict;
    int fd;
    int has_previous_json = 0;

    /* Initialize a dictionary */
    dict_init(&dict);

    /* Open binary file for writing */
    fd = open(bin_file, O_CREAT | O_TRUNC | O_WRONLY, 0644);
    if (fd < 0)
        err_sys("Unable to write to binary file");

    /* Open JSON file for reading */
    json = json_open(json_file);
    if (json == NULL)
        err_sys(json_strerr(json_error));

    while (!json->end_of_file) {

        /* Read all K/V pairs from JSON */
        json_reset(json);

        if (has_previous_json)
            kv_write_separator(fd);

        while (!json->end_of_json && !json->end_of_file && (kv = json_read(json)) != NULL) {

            /* Add key to dictionary, get key index */
            kv->idx_key = dict_add(&dict, kv->key, -1);
            if (kv->idx_key == -1)
                err_sys("Dictionary memory error");
            kv->key = NULL;

            /* Write indexed K/V pair to a file */
            kv_write(fd, kv);
        }

        if (!json->end_of_json && !json->end_of_file && kv == NULL)
            err_sys(json_strerr(json_error));

        has_previous_json = 1;
    }

    close(fd);

    printf("JSON file encoded\n");

    /* Write dictionary to a file */
    fd = open(dict_file, O_CREAT | O_TRUNC | O_WRONLY, 0644);
    if (fd < 0)
        err_sys("Unable to write to dictionary file");

    if (dict_write(fd, &dict) < 0)
        err_sys("Unable to write to dictionary file");

    close(fd);

    printf("Dictionary file saved\n");

    json_close(json);
    dict_destroy(&dict);
}


void json_decode(const char *json_file, const char *bin_file, const char *dict_file)
{
    int fd, res, first_element;
    kvpair_t kv;
    keydict_t dict;
    char *key;
    FILE *json;

    /* Read dictionary from a file */
    fd = open(dict_file, O_RDONLY);
    if (fd < 0)
        err_sys("Unable to read from dictionary file");
    if (dict_read(fd, &dict) < 0)
        err_sys("Unable to read from dictionary file");
    close(fd);

    printf("Dictionary file loaded\n");

    /* Open binary file for reading */
    fd = open(bin_file, O_RDONLY);
    if (fd < 0)
        err_sys("Unable to read from binary file");

    /* Open JSON file for writing */
    json = fopen(json_file, "w");
    if (json == NULL)
        err_sys("Unable to write to JSON file");

    do {

        /* Write formatted JSON file from binary file */
        first_element = 1;
        while ((res = kv_read(fd, &kv)) > 0) {
            key = dict_get(&dict, kv.idx_key);
            if (key == NULL)
                err_sys("Invalid KV key index");
            kv.key = key;
            if (!first_element)
                fprintf(json, ", ");
            else {
                fprintf(json, "{ ");
                first_element = 0;
            }
            json_print_kv(json, &kv);
        }
        if (!first_element)
            fprintf(json, " }\n");

    } while (res == 0);

    fclose(json);

    printf("JSON decoded\n");

    close(fd);
}


void err_sys(const char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(-1);
}
