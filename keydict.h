#ifndef __keydict_h__
#define __keydict_h__

#include <stddef.h>

/* Key-value pair */
typedef struct {
    char *key;
    int index;
} keyitem_t;

/* Dictionary of indexed keys */
typedef struct {
    keyitem_t *keys;
    size_t size;
    int last_index;
} keydict_t;

/* Initialize a dictionary */
void dict_init(keydict_t *dict);

/* Free dictionary memory */
void dict_destroy(keydict_t *dict);

/* Add a new key to a dictionary */
int dict_add(keydict_t *dict, char *key, int index);

/* Write dictionary to a file */
int dict_write(int fd, keydict_t *dict);

/* Read dictionary from a file */
int dict_read(int fd, keydict_t *dict);

/* Get key string from a dictionary by its index */
char *dict_get(keydict_t *dict, int index);

#endif /* keydict.h */
